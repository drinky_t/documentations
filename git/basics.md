# Git Basics

## Clone an existing git repo
`git clone <url>/<username>/<projectname>.git`

## Push local changes to the git server
Make sure to be at the most upper directory of the git project

Add all files that have changed locally:

`git add .`

Insert a commit message:

`git commit -m "<my message>"`

Push the selected changes to the server:

`git push`

## Pull changes from your git server, to your local filesystem
`git pull`
